/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author OMEN
 */
public class TestTable {

    public TestTable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRow2ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRow3ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRow1ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testRow2ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testRow3ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testCol1ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testCol2ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testCol3ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testCol1ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testCol2ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testCol3ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testsetRowColisNotEmpty() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testsetRowColisNotEmptyO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(0, 0);
        assertEquals(false, table.setRowCol(0, 0));
    }

    @Test
    public void testisFinishX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }
     @Test
    public void testisFinishO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }
     @Test
    public void testisFinishByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }
    public void testisFinishByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }

}
